#include <stdio.h>
#define ARRAY_LENGTH 10

int array[ARRAY_LENGTH];
int swapArray[ARRAY_LENGTH];

void printArray()
{
    for (int i = 0; i < ARRAY_LENGTH; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");
}

int quickSort(int anfang, int ende, int prevPivot)
{
    int pivotIndex = (ende - anfang) / 2 + anfang;
    int pivot = array[pivotIndex];

    int anfangDerErstenListe = anfang;
    int endeDerErstenListe = pivotIndex - 1;    //placeholder
    int anfangDerZweitenListe = pivotIndex + 1; //placeholder
    int endeDerZweitenListe = ende;
    // sortieren
    if (ende - anfang >= 1)
    {
        printArray();

        // initialize swap-array
        int placeSwap = 0;
        for (int i = 0; i < ARRAY_LENGTH; i++) // TODO does not have to loop the entire list
        {
            if (array[i] > pivot)
            {
                swapArray[placeSwap] = array[i];
                placeSwap++;
            }
        }

        // get new pivot
        placeSwap = 0;
        int smaller = 0;
        for (int i = 0; i < ARRAY_LENGTH; i++) // TODO does not have to loop the entire list
        {
            if (array[i] < pivot)
            {
                array[smaller] = array[i];
                smaller++;
            }
        }

        array[smaller] = pivot;
        prevPivot = smaller;
        endeDerErstenListe = prevPivot - 1;
        anfangDerZweitenListe = prevPivot + 1;
        smaller++;

        for (int i = smaller; i < ARRAY_LENGTH; i++) // TODO does not have to loop the entire list
        {
            array[i] = swapArray[placeSwap];
            placeSwap++;
        }
        placeSwap = 0;

        // splitten am pivot
        quickSort(anfangDerErstenListe, endeDerErstenListe, pivotIndex);
        quickSort(anfangDerZweitenListe, endeDerZweitenListe, pivotIndex);
    }
    else
    {
        return 1;
    }
}

int main()
{
    int fertig = 0;
    array[0] = 5;
    array[1] = 9;
    array[2] = 10;
    array[3] = 6;
    array[4] = 7;
    array[5] = 4;
    array[6] = 2;
    array[7] = 1;
    array[8] = 3;
    array[9] = 8;

    printArray();
    while (fertig != 1)
    {
        fertig = quickSort(0, ARRAY_LENGTH - 1, -1);
    }
    printArray();

    return 0;
}